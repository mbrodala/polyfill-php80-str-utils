<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-php80-str-utils library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Polyfill\StrUtils;

if(!\function_exists('str_contains'))
{
	
	/**
	 * Determine if a string contains a given substring, case sensitive.
	 * 
	 * @link https://www.php.net/manual/en/function.str-contains.php
	 * @param string $haystack The string to search in.
	 * @param string $needle The substring to search for in the haystack. 
	 * @return boolean
	 */
	function str_contains(?string $haystack, ?string $needle) : bool
	{
		return StrUtils::strContains($haystack, $needle);
	}
}

if(!\function_exists('str_icontains'))
{
	
	/**
	 * Determine if a string contains a given substring, case insensitive.
	 * 
	 * @param string $haystack The string to search in.
	 * @param string $needle The substring to search for in the haystack. 
	 * @return boolean
	 */
	function str_icontains(?string $haystack, ?string $needle) : bool
	{
		return StrUtils::striContains($haystack, $needle);
	}
}

if(!\function_exists('str_starts_with'))
{
	
	/**
	 * Checks if a string starts with a given substring, case sensitive.
	 * 
	 * @link https://www.php.net/manual/en/function.str-starts-with.php
	 * @param string $haystack The string to search in.
	 * @param string $needle The substring to search for in the haystack.
	 * @return boolean
	 */
	function str_starts_with(?string $haystack, ?string $needle) : bool
	{
		return StrUtils::strStartsWith($haystack, $needle);
	}
}

if(!\function_exists('str_istarts_with'))
{
	
	/**
	 * Checks if a string starts with a given substring, case insensitive.
	 * 
	 * @param string $haystack The string to search in.
	 * @param string $needle The substring to search for in the haystack.
	 * @return boolean
	 */
	function str_istarts_with(?string $haystack, ?string $needle) : bool
	{
		return StrUtils::striStartsWith($haystack, $needle);
	}
}

if(!\function_exists('str_starts_with'))
{
	
	/**
	 * Checks if a string ends with a given substring, case sensitive.
	 *
	 * @link https://www.php.net/manual/en/function.str-ends-with.php
	 * @param string $haystack The string to search in.
	 * @param string $needle The substring to search for in the haystack.
	 * @return boolean
	 */
	function str_ends_with(?string $haystack, ?string $needle) : bool
	{
		return StrUtils::strEndsWith($haystack, $needle);
	}
}

if(!\function_exists('str_iends_with'))
{
	
	/**
	 * Checks if a string ends with a given substring, case insensitive.
	 * 
	 * @param string $haystack
	 * @param string $needle
	 * @return boolean
	 */
	function str_iends_with(?string $haystack, ?string $needle) : bool
	{
		return StrUtils::striEndsWith($haystack, $needle);
	}
}
